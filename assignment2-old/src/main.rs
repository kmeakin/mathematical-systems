#![warn(
    clippy::all,
    clippy::nursery,
    clippy::pedantic,
    missing_copy_implementations,
    missing_debug_implementations,
    rust_2018_idioms,
    unused_qualifications
)]
#![allow(clippy::must_use_candidate)]
#![allow(clippy::cast_sign_loss)]
#![allow(clippy::cast_possible_truncation)]
#![allow(clippy::suspicious_operation_groupings)]
#![feature(format_args_capture)]

use std::{
    fmt,
    ops::{Index, IndexMut},
};

const INTIAL_TEMP: f64 = 500.0;

const LENGTH: f64 = 100.0;
const TIME: f64 = 600.0;

const K: f64 = 0.835;

#[cfg(feature = "a")]
const TIME_STEP: f64 = 100.0;
#[cfg(feature = "a")]
const LENGTH_STEP: f64 = 20.0;

#[cfg(feature = "b")]
const TIME_STEP: f64 = 50.0;
#[cfg(feature = "b")]
const LENGTH_STEP: f64 = 20.0;

#[cfg(feature = "c")]
const TIME_STEP: f64 = 100.0;
#[cfg(feature = "c")]
const LENGTH_STEP: f64 = 10.0;

#[cfg(feature = "d")]
const TIME_STEP: f64 = 50.0;
#[cfg(feature = "d")]
const LENGTH_STEP: f64 = 10.0;

const N_COLS: usize = (LENGTH / LENGTH_STEP) as usize + 1;
const N_ROWS: usize = (TIME / TIME_STEP) as usize + 1;

pub fn analytical(x: f64, t: f64) -> f64 {
    let pi = std::f64::consts::PI;
    let coeff = 2000.0 / pi;
    let sum: f64 = (0..1_000_000)
        .map(|n| {
            let n = n * 2 + 1;
            let n = f64::from(n);
            let sin = n * pi * x / LENGTH;
            let exp = -(n * n * pi * pi * K) / (LENGTH * LENGTH) * t;
            1.0 / n * sin.sin() * exp.exp()
        })
        .sum();
    coeff * sum
}

#[derive(Copy, Clone)]
pub struct Grid {
    grid: [[f64; N_COLS]; N_ROWS],
}

impl Default for Grid {
    fn default() -> Self {
        let mut grid = [[INTIAL_TEMP; N_COLS]; N_ROWS];
        for row in &mut grid {
            row[0] = 0.0;
            row[N_COLS - 1] = 0.0;
        }
        Self { grid }
    }
}

impl Grid {
    pub fn new() -> Self { Self::default() }

    pub const fn get(&self, i: usize, j: usize) -> f64 { self.grid[j][i] }
    pub fn set(&mut self, i: usize, j: usize, val: f64) { self.grid[j][i] = val }

    pub fn update_equation(&self, i: usize, j: usize) -> f64 {
        (K * TIME_STEP / (LENGTH_STEP * LENGTH_STEP)).mul_add(
            self.get(i - 1, j - 1) - 2.0 * self.get(i, j - 1) + self.get(i + 1, j - 1),
            self.get(i, j - 1),
        )
    }
}

impl Index<(usize, usize)> for Grid {
    type Output = f64;
    fn index(&self, (x, t): (usize, usize)) -> &Self::Output { &self.grid[t][x] }
}
impl IndexMut<(usize, usize)> for Grid {
    fn index_mut(&mut self, (x, t): (usize, usize)) -> &mut Self::Output { &mut self.grid[t][x] }
}

impl fmt::Debug for Grid {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for row in &self.grid {
            writeln!(f)?;
            write!(f, "{:?}", row)?;
        }
        Ok(())
    }
}

fn main() {
    let mut grid = Grid::new();
    dbg!(grid);

    for j in 1..N_ROWS {
        for i in 1..N_COLS - 1 {
            let new_temp = grid.update_equation(i, j);
            grid.set(i, j, new_temp);
        }
    }

    dbg!(grid);

    for t in 0..N_ROWS {
        let t = t as f64 * TIME_STEP;
        let val = analytical(20.0, t as f64);
        println!("{}", val);
    }
}
