---
title: Mathemtical Systems, exercise 2
author: Karl Meakin
geometry: margin=1cm
output:
  pdf_document:
    highlight: pygments
    latex_engine: pdflatex
    pandoc_args: "--pdf-engine-opt=--shell-escape"
header-includes:
- \usepackage{svg}
---

# Part 1
## 1.1
Derivation of update equations:

$\frac{\partial^{2} T(x,t)}{\partial x^{2}} = \frac{1}{k} \frac{\partial T(x,t)}{\partial t}$

$\frac{\partial^{2} T(x,t)}{\partial x^{2}}|_{i,j} \approx \frac{T(i-1,j) - 2T(i,j)+T(i+1,j)}{(\Delta x)^{2}}$

$\frac{\partial T(x,t)}{\partial t}|_{i,j} \approx \frac{T(i,j+1)-T(i,j)}{\Delta t}$

$\frac{T(i-1,j) - 2T(i,j) + T(i+1,j)}{(\Delta x)^{2}} = \frac{1}{k} \frac{T(i,j+1) - T(i,j)}{\Delta t}$

$T(i,j+1) = \frac{k \Delta t}{(\Delta x)^{2}}[T(i-1,j) - 2T(i,j) + T(i+1,j)] + T(i,j)$

$T(i,j) = \frac{k \Delta t}{(\Delta x)^{2}}[T(i-1,j-1) - 2T(i,j-1) + T(i+1,j-1)] + T(i,j-1)$

## 1.2
## 1.3
## 1.4
