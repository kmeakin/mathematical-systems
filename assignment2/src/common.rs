use std::{
    fmt, iter,
    ops::{Index, IndexMut},
};

pub const LENGTH: f64 = 100.0;
pub const TIME: f64 = 600.0;
pub const K: f64 = 0.835;
pub const INITIAL_TEMP: f64 = 500.0;

pub fn analytical(x: f64, t: f64) -> f64 {
    let pi = std::f64::consts::PI;
    let sum: f64 = (0..1_000)
        .map(|n| {
            let n = n * 2 + 1;
            let n = f64::from(n);
            let sin = n * pi * x / LENGTH;
            let exp = -(n * n * pi * pi * K) / (LENGTH * LENGTH) * t;
            1.0 / n * sin.sin() * exp.exp()
        })
        .sum();
    2000.0 * sum / pi
}

#[derive(Clone)]
pub struct Grid {
    grid: Vec<Vec<f64>>,
}

impl Grid {
    fn new_inner(n_rows: usize, n_cols: usize) -> Self {
        let mut grid = iter::repeat(iter::repeat(INITIAL_TEMP).take(n_cols).collect::<Vec<_>>())
            .take(n_rows)
            .collect::<Vec<_>>();
        for row in &mut grid {
            row[0] = 0.0;
            row[n_cols - 1] = 0.0;
        }
        Self { grid }
    }

    pub fn new(length_step: f64, time_step: f64) -> Self {
        let n_rows: usize = (TIME / time_step) as usize + 1;
        let n_cols: usize = (LENGTH / length_step) as usize + 1;
        Self::new_inner(n_rows, n_cols)
    }

    pub fn explicit(&self, time_step: f64, length_step: f64, i: usize, j: usize) -> f64 {
        let gamma = (K * time_step) / (length_step * length_step);
        let j = j - 1;
        (1.0 - 2.0 * gamma) * self[(i, j)] + gamma * (self[(i - 1, j)] + self[(i + 1, j)])
    }

    pub fn n_rows(&self) -> usize { self.grid.len() }
    pub fn n_cols(&self) -> usize { self.grid[0].len() }

    pub fn length_step(&self) -> f64 { LENGTH / (self.n_cols() as f64 - 1.0) }
    pub fn time_step(&self) -> f64 { TIME / (self.n_rows() as f64 - 1.0) }
}

impl fmt::Debug for Grid {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for row in &self.grid {
            writeln!(f)?;
            write!(f, "{:?}", row)?;
        }
        Ok(())
    }
}

impl Index<(usize, usize)> for Grid {
    type Output = f64;
    fn index(&self, (x, t): (usize, usize)) -> &Self::Output { &self.grid[t][x] }
}
impl IndexMut<(usize, usize)> for Grid {
    fn index_mut(&mut self, (x, t): (usize, usize)) -> &mut Self::Output { &mut self.grid[t][x] }
}

impl Index<(f64, f64)> for Grid {
    type Output = f64;
    fn index(&self, (x, t): (f64, f64)) -> &Self::Output {
        let i = (x / self.length_step()) as usize;
        let j = (t / self.time_step()) as usize;
        &self[(i, j)]
    }
}
