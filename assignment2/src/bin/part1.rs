use std::error::Error;

use assignment2::common::*;
use plotters::prelude::*;
use plotters_bitmap::*;

fn temperature_distribution(length_step: f64, time_step: f64) -> Grid {
    let mut grid = Grid::new(length_step, time_step);

    for j in 1..grid.n_rows() {
        for i in 1..grid.n_cols() - 1 {
            let new_temp = grid.explicit(time_step, length_step, i, j);
            grid[(i, j)] = new_temp;
        }
    }

    grid
}

fn temperature_evolution(grid: &Grid) -> Vec<(f64, f64, f64)> {
    let x = 20.0;
    let mut v = Vec::new();
    for t in 0..grid.n_rows() {
        let t = t as f64 * grid.time_step();
        let numeric = grid[(x, t)];
        let analytic = analytical(x, t);
        v.push((numeric, analytic, numeric - analytic));
    }
    v
}

fn plot_temperature_distribution(grid: &Grid) -> Result<(), Box<dyn Error>> {
    let root =
        plotters_bitmap::BitMapBackend::new("img/temperature_distribution.png", (1920, 1080))
            .into_drawing_area();
    root.fill(&WHITE)?;
    let mut chart = ChartBuilder::on(&root)
        .caption("Temperature distribution", ("sans-serif", 50).into_font())
        .margin(5)
        .x_label_area_size(30)
        .y_label_area_size(30)
        .build_cartesian_2d(0.0..LENGTH, 0.0..TIME)?;

    chart
        .configure_mesh()
        .x_desc("Distance (cm)")
        .y_desc("Temperature (C)")
        .draw()?;

    const colors: &[RGBColor] = &[RED, GREEN, BLUE, YELLOW, CYAN, MAGENTA];

    for t in 0..grid.n_cols() {
        let color = &colors[t];
        let time = t as f64 * grid.time_step();
        chart
            .draw_series(LineSeries::new(
                (0..grid.n_rows() - 1).map(|x| {
                    let x = x as f64 * grid.length_step();
                    let temp = grid[(x, time)];
                    (x, temp)
                }),
                color,
            ))?
            .label(format!("t = {}", time))
            .legend(move |(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], color));
    }

    chart
        .configure_series_labels()
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .draw()?;

    Ok(())
}

fn plot_temperature_evolution(
    grid_a: &Grid,
    grid_b: &Grid,
    grid_c: &Grid,
) -> Result<(), Box<dyn Error>> {
    fn draw<'a>(
        chart: &mut ChartContext<
            'a,
            BitMapBackend,
            Cartesian2d<
                plotters::coord::types::RangedCoordf64,
                plotters::coord::types::RangedCoordf64,
            >,
        >,
        grid: &Grid,
        color: &'a RGBColor,
    ) -> Result<(), Box<dyn Error>> {
        chart
            .draw_series(LineSeries::new(
                (0..grid.n_rows() - 0).map(|t| {
                    let time = t as f64 * grid.time_step();
                    let x = 20.0;
                    let temp = grid[(x, time)];
                    (time, temp)
                }),
                color,
            ))?
            .label(format!(
                "Δt = {}s, Δx = {}s",
                grid.time_step(),
                grid.length_step()
            ))
            .legend(move |(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], color));
        Ok(())
    }

    let root = plotters_bitmap::BitMapBackend::new("img/temperature_evolution.png", (1920, 1080))
        .into_drawing_area();
    root.fill(&WHITE)?;
    let mut chart = ChartBuilder::on(&root)
        .caption("Temperature evolution", ("sans-serif", 50).into_font())
        .margin(5)
        .x_label_area_size(30)
        .y_label_area_size(30)
        .build_cartesian_2d(0.0..TIME, 0.0..TIME)?;

    chart
        .configure_mesh()
        .x_desc("Time (s)")
        .y_desc("Temperature (C)")
        .draw()?;

    chart
        .draw_series(LineSeries::new(
            (0..TIME as u32).map(|time| {
                let time = time as f64;
                let x = 20.0;
                let temp = analytical(x, time);
                (time, temp)
            }),
            &GREEN,
        ))?
        .label(format!("analytical"))
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &GREEN));

    draw(&mut chart, grid_a, &RED)?;
    draw(&mut chart, grid_b, &BLUE)?;
    draw(&mut chart, grid_c, &MAGENTA)?;

    chart
        .configure_series_labels()
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .draw()?;

    Ok(())
}

fn main() {
    let grid_a = temperature_distribution(20.0, 100.0);
    let temp_a = grid_a[(20.0, 600.0)];
    plot_temperature_distribution(&grid_a).unwrap();

    let grid_b = temperature_distribution(20.0, 50.0);
    let temp_b = grid_b[(20.0, 600.0)];

    let grid_c = temperature_distribution(10.0, 100.0);
    let temp_c = grid_c[(20.0, 600.0)];

    plot_temperature_evolution(&grid_a, &grid_b, &grid_c).unwrap();
}
