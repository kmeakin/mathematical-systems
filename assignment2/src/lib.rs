#![warn(
    clippy::all,
    clippy::nursery,
    clippy::pedantic,
    missing_copy_implementations,
    missing_debug_implementations,
    rust_2018_idioms,
    unused_qualifications
)]
#![allow(
    clippy::must_use_candidate,
    clippy::suboptimal_flops,
    clippy::suspicious_operation_groupings
)]

pub mod common;
