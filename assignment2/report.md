---
monofont: 'Fira Mono'
geometry: margin=1cm
---
The equation to be solved:
$$
\frac{\partial^{2} T(x, t)}{\partial x^{2}} = \frac{1}{k} \frac{\partial T(x,
t)}{\partial t}
$$

# Part 1
## 1.1

Central difference equation of the second-order spatial derivative:
$$ 
\frac{\partial^{2} T(x, t)}{\partial x^{2}}\bigg{|}_{i, j} \approx \frac{T(i-1, j) +
T(i + 1, j)}{(\Delta x)^{2}} 
$$

Forward difference approximation of the first-order time deriviative:
$$
\frac{\partial T(x, t)}{\partial t}\bigg{|}_{i, j} \approx \frac{T(i, j+1) - T(i, j)}{\Delta t}
$$

Update equation:
$$
T(i, j+1) = (1-2 \gamma) T(i, j) + \gamma (T(i-1, j) + T(i + 1, j))
$$

where $\gamma = \frac{k \Delta t}{(\Delta x)^{2}}$

## 1.2
TODO: use same notation as part 1.1

Computational molecule
![](img/1.2-computational-molecule.png)

## 1.3
TODO: discuss implementation

## 1.4
TODO: plots

## 1.5
TODO: plots

# Part 2
## 1.2
Finite difference equations:
$$
\frac{\partial T(x, t)}{\partial t}\bigg{|}_{i, j + \frac{1}{2}} \approx
\frac{T(i, j + 1) - T(i, j)}{\Delta t}
$$

$$
\frac{\partial^{2} T(x, t)}{\partial x^{2}}\bigg{|}_{i, j + \frac{1}{2}} \approx
\frac{T(i-1, j) - 2T(i, j) + T(i + 1, j) + T(i-1 ,j+1) - 2T(i ,j+1) + T(i+1,
j+1)}{2 (\Delta x)^{2}}
$$

Update equation:
$$
T(i, j) = \frac{(1 + \gamma) T(i, j+1) - \frac{\gamma}{2} (T(i-1, j) + T(i+1, j)
+ T(i+1, j+1))}{1 - \gamma}
$$

where $\gamma = \frac{\Delta t}{k (\Delta x)^{2}}$

# Part 3
