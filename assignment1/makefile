CFLAGS:=-std=c99 -Wall -Wextra -Wpedantic -O3
LDFLAGS:=-lm

all: build

SRCS:=src/common.c src/part1.c src/part2.c src/part3.c src/main.c
OBJS:=out/common.o out/part1.o out/part2.o out/part3.o out/main.o
PNGS:=out/part1.png out/part2.png out/part3.png

out/%.o: src/%.c
	@ mkdir -p out
	$(CC) $(CFLAGS) -c -o $@ $<

build: $(OBJS)
	$(CC) $(LDFLAGS) $(OBJS) -o out/assignment1

run: build
	./out/assignment1

out/part1.png:
	gnuplot -e "set autoscale;\
				set term png;\
				set xlabel 'Iterations';\
				set ylabel 'Absolute Error';\
				set title 'Part 1';\
				set output 'out/part1.png';\
				plot 'out/part1.csv' using 1:2 title 'Stochastic Rounding' with line"

out/part2.png:
	gnuplot -e "set autoscale;\
				set term png;\
				set logscale xy;\
				set xlabel 'Iterations';\
				set ylabel 'Absolute Error';\
				set title 'Part 2';\
				set output 'out/part2.png';\
				plot 'out/harmonic_f32_rn.csv' using 1:2 title 'Binary32 Recursive Summation' with lines ls 1, 'out/harmonic_f32_sr.csv' using 1:2 title 'Binary32 Stochastic Rounding' with lines ls 2, 'out/harmonic_f32_comp.csv' using 1:2 title 'Binary32 Compensated Summation' with lines ls 3;\
				"

out/part3.png:
	gnuplot -e "set autoscale;\
				set term png;\
				set logscale xy;\
				set xlabel 'Iterations';\
				set ylabel 'Absolute Error';\
				set title 'Part 3';\
				set output 'out/part3.png';\
				plot 'out/basel_f32_rn.csv' using 1:2 title 'Binary32 Recursive Summation' with lines ls 1, 'out/basel_f32_sr.csv' using 1:2 title 'Binary32 Stochastic Rounding' with lines ls 2, 'out/basel_f32_comp.csv' using 1:2 title 'Binary32 Compensated Summation' with lines ls 3;\
				"

doc: $(PNGS)
	Rscript -e 'rmarkdown::render("src/report.Rmd")'
	mv src/report.pdf report.pdf

zip: doc
	zip assignment1.zip -r src makefile report.pdf

clean:
	rm -rf out
