let
  pkgs = import <nixpkgs> {};
in
pkgs.mkShell {
  buildInputs = with pkgs; [
    (rWrapper.override { packages = with rPackages; [ rmarkdown ]; })
    pandoc
    texlive.combined.scheme-full
    gnuplot
    zip
  ];
}
