#include "common.h"
#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>

f64 harmonic_series[N / STEP] = {0};

f32 harmonic_f32_rn(int n) {
  FILE *fp = open_file("out/harmonic_f32_rn.csv");

  f32 s = 0.0f;
  f32 old_s;
  bool stagnated = false;
  for (int i = 1; i <= n; i++) {
    old_s = s;
    s += inv_f32(i);
    if (!stagnated && old_s == s) {
      stagnated = true;
      printf("harmonic sum stagnates at i = %d (s = %.60f)\n", i, s);
    }
    print_result(fp, harmonic_series, i, s);
  }
  return s;
}

f64 harmonic_f64_rn(int n) {
  f64 s = 0.0;
  for (int i = 1; i <= n; i++) {
    s += inv_f64(i);

    if (i % STEP == 0) {
      harmonic_series[i / STEP] = s;
    }
  }
  return s;
}

f32 harmonic_f32_sr(int n) {
  FILE *fp = open_file("out/harmonic_f32_sr.csv");

  f32 s = 0.0f;
  for (int i = 1; i <= n; i++) {
    s = SR((double)s + inv_f64(i));
    print_result(fp, harmonic_series, i, s);
  }
  return s;
}

f32 harmonic_f32_comp(int n) {
  FILE *fp = open_file("out/harmonic_f32_comp.csv");

  f32 s = 1.0f;
  f32 t = 0.0;
  for (int i = 2; i <= n; i++) {
    t = t + inv_f32(i);
    Pair pair = fast2sum(s, t);
    s = pair.s;
    t = pair.t;
    print_result(fp, harmonic_series, i, s);
  }
  return s;
}

void part2() {
  f64 f64_rn = harmonic_f64_rn(N);

  f32 f32_rn = harmonic_f32_rn(N);
  f32 f32_sr = harmonic_f32_sr(N);
  f32 f32_comp = harmonic_f32_comp(N);

  printf("Values of the harmonic series after %d iterations \n", N);
  printf("Recursive summation, binary32:          %.30f (err = %.30f)\n",
         f32_rn, f64_rn - f32_rn);
  printf("Recursive summation with SR, binary32:  %.30f  (err = %.30f)\n",
         f32_sr, f64_rn - f32_sr);
  printf("Compensated summation, binary32:        %.30f  (err = %.30f)\n",
         f32_comp, f64_rn - f32_comp);
  printf("Recursive summation, binary64:          %.30f\n", f64_rn);
}
