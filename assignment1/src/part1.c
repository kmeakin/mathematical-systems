#define _POSIX_SOURCE
#define _XOPEN_SOURCE 500

#include "common.h"
#include <math.h>
#include <stdint.h>
#include <stdlib.h>

const f64 SAMPLE = M_PI;
const int N_ROUNDS = 5000000;

void part1() {
  FILE *results_fp = open_file("out/part1.csv");
  Estimates es = estimates(SAMPLE);

  f64 avg = 0.0;
  int n_rounds_up = 0;
  int n_rounds_down = 0;
  for (int i = 1; i <= N_ROUNDS; i++) {
    f64 rounded = SR_alternative(SAMPLE);
    avg += rounded;
    if (rounded > SAMPLE) {
      n_rounds_up++;
    } else {
      n_rounds_down++;
    }
    if (i % 1000 == 0) {
      f64 avg_so_far = avg / i;
      f64 err = SAMPLE - avg_so_far;
      fprintf(results_fp, "%d\t%.60f\n", i, err);
    }
  }
  avg /= N_ROUNDS;

  // Print out some useful stats.
  printf("Value being rounded:   %.60f \n", SAMPLE);
  printf("Binary32 value before: %.60f \n", es.under);
  printf("Binary32 value after:  %.60f \n", es.over);
  printf("Closest binary32:      %.60f \n", es.closest);

  // Print out the average of all rounded values
  printf("Avergage over %d iterations: %.60f \n", N_ROUNDS, avg);
  printf("Number of times rounded up: %d \n", n_rounds_up);
  printf("Number of times rounded down: %d \n", n_rounds_down);

  // Check that SR function is correct by comparing the probabilities
  // of rounding up/down, and the expected probability. Print them out
  // below.
  f64 p = fabs(SAMPLE - es.under) / fabs(es.over - es.under);
  printf("Expected probability of rounding up:\t%.60f \n", p);
  printf("Observed probability of rounding up:\t%.60f \n",
         (f32)n_rounds_up / N_ROUNDS);

  printf("Expected probability of rounding down:\t%.60f \n", 1.0 - p);
  printf("Observed probability of rounding down:\t%.60f \n",
         (f32)n_rounds_down / N_ROUNDS);
}
