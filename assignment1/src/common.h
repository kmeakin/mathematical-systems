#pragma once

#include <stdint.h>
#include <stdio.h>

// These should be `const int`, but gcc doesn't accept variable references in
// array sizes, even if the variables are constants
#define N 500000000
#define STEP 1000000

typedef float f32;
typedef double f64;

f32 inv_f32(int x);
f64 inv_f64(int x);

f32 inv2_f32(int x);
f64 inv2_f64(int x);

typedef struct {
  f32 closest;
  f32 over;
  f32 under;
} Estimates;

Estimates estimates(f64 x);
f32 SR(f64 x);
f32 SR_alternative(f64 x);

typedef struct {
  f32 s;
  f32 t;
} Pair;

Pair fast2sum(f32 a, f32 b);
void print_result(FILE *fp, f64 *table, int i, f32 approx_val);

FILE *open_file(const char *path);
