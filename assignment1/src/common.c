#include "common.h"
#include <assert.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>

f32 inv_f32(int x) { return 1.0f / (f32)x; }
f64 inv_f64(int x) { return 1.0 / (f64)x; }

f32 inv2_f32(int x) { return inv_f32(x) * inv_f32(x); }
f64 inv2_f64(int x) { return inv_f64(x) * inv_f64(x); }

Estimates estimates(f64 x) {
  f32 closest = (f32)x;
  f32 over, under;
  if (closest > x) {
    under = nextafterf(closest, -INFINITY);
    over = closest;
  } else {
    over = nextafterf(closest, +INFINITY);
    under = closest;
  }
  return (Estimates){.closest = closest, .over = over, .under = under};
}

// A function that rounds a binary64 value to a binary32 value
// stochastically. Implemented by treating FP number representations
// as integer values.
f32 SR(f64 x) {
  uint64_t temp = *(uint64_t *)&x;
  uint32_t r = (rand() * (0xFFFFFFFF / RAND_MAX)) % 0x1FFFFFFF;
  temp += r;
  temp = temp & 0xFFFFFFFFE0000000;
  return (f32)*(f64 *)&temp;
}

// A function that rounds a binary64 value to a binary32 value
// stochastically. Implemented by working at the FP arithmetic level rather
// than manipulating the binary representation of FP numbers directly.
f32 SR_alternative(f64 x) {
  Estimates es = estimates(x);
  f32 over = es.over;
  f32 under = es.under;

  f64 p = fabs(x - under) / fabs(over - under);
  f64 r = (f64)rand() / RAND_MAX;

  return (r < p) ? over : under;
}

Pair fast2sum(f32 a_, f32 b_) {
  f32 a = fmaxf(a_, b_);
  f32 b = fminf(a_, b_);
  assert(fabs(a) >= fabs(b));
  f32 s = a + b;
  f32 b_prime = s - a;
  f32 t = b - b_prime;
  return (Pair){.s = s, .t = t};
}

void print_result(FILE *fp, f64 *table, int i, f32 approx_val) {
  if (i % STEP == 0) {
    f64 accurate_val = table[i / STEP];
    f64 err = fabs(accurate_val - approx_val);
    fprintf(fp, "%i\t%.60f\n", i, err);
  }
}

FILE *open_file(const char *path) {
  FILE *fp = fopen(path, "w");
  if (fp == NULL) {
    fprintf(stderr, "Could not open %s\n", path);
    exit(1);
  }
  return fp;
}
